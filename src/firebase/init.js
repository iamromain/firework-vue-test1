  import firebase from 'firebase'
  import firestore from 'firebase/firestore'
  
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyD54bbp1qLbniwwV7fpMYdN0hh4XboY-Sw",
    authDomain: "udemy-1-ninja.firebaseapp.com",
    databaseURL: "https://udemy-1-ninja.firebaseio.com",
    projectId: "udemy-1-ninja",
    storageBucket: "udemy-1-ninja.appspot.com",
    messagingSenderId: "921872099566"
  };
 const firebaseApp =  firebase.initializeApp(config);

 // export firebase database

 export default firebaseApp.firestore()